package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CMenu;
import com.example.demo.Respository.iMenuRespository;

@RestController
@CrossOrigin
@RequestMapping("menu")
public class CMenuController {

    
    @Autowired
    iMenuRespository iMenuRespository;

    // lấy danh sách all menu
    @GetMapping("/all")
    public ResponseEntity <List <CMenu>> getMenu(){

        try {
            List<CMenu> listMenu = new ArrayList<CMenu>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            iMenuRespository.findAll().forEach(listMenu :: add);

            if(listMenu.size() == 0){
                return new ResponseEntity<List <CMenu>>(listMenu, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <CMenu>>(listMenu, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tìm menu bởi id
    @GetMapping("/{id}")
	public ResponseEntity<Object> getMenuById(@PathVariable(name = "id") long id){
		Optional<CMenu> menuFounded = iMenuRespository.findById(id);
		if(menuFounded.isPresent()){
			return new ResponseEntity<Object>(menuFounded, HttpStatus.OK);
		} 
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

    // tạo menu
	@PostMapping("/create")
	public ResponseEntity<Object> createNewMenu(@RequestBody CMenu newMennu){
		try {
			CMenu newmenu = new CMenu(newMennu.getSize(), newMennu.getDuongKinh(), newMennu.getSuon(), newMennu.getSalad(), newMennu.getSoLuongNuocNgot(), newMennu.getDonGia());
			return new ResponseEntity<Object>(iMenuRespository.save(newmenu), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    // update bởi id
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateMenuById(@PathVariable(name = "id") long id, @RequestBody CMenu newMennu){
		try {
			Optional<CMenu> menuFounded = iMenuRespository.findById(id);
			if(menuFounded.isPresent()){
				CMenu menuData = menuFounded.get();
				menuData.setDonGia(newMennu.getDonGia());
				menuData.setDuongKinh(newMennu.getDuongKinh());
				menuData.setSalad(newMennu.getSalad());
				menuData.setSize(newMennu.getSize());
				menuData.setSoLuongNuocNgot(newMennu.getSoLuongNuocNgot());
				menuData.setSuon(newMennu.getSuon());
				return new ResponseEntity<Object>(iMenuRespository.save(menuData), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteMenuById(@PathVariable(name = "id") Long id){
		try {
			Optional<CMenu> menuFounded = iMenuRespository.findById(id);
			if(menuFounded.isPresent()){
				iMenuRespository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
