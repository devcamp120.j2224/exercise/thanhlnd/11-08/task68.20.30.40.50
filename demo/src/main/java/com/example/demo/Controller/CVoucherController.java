package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CVoucher;
import com.example.demo.Respository.iVoucherRespository;

@RestController
@CrossOrigin
@RequestMapping("voucher")
public class CVoucherController {

    @Autowired
    iVoucherRespository iVoucherRepository;


    // lấy danh sách all voucher
    @GetMapping("/all")
    public ResponseEntity <List <CVoucher>> getVouchers(){

        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
       
            iVoucherRepository.findAll().forEach(listVoucher :: add);

            if(listVoucher.size() == 0){
                return new ResponseEntity<List <CVoucher>>(listVoucher, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <CVoucher>>(listVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tìm voucher by id 
    @GetMapping("/{id}")
    public ResponseEntity<CVoucher> getVoucherById(@PathVariable("id") Long id) {
        Optional<CVoucher> voucher = iVoucherRepository.findById(id);
        if (voucher.isPresent()) {
            return new ResponseEntity<>(voucher.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tạo voucher
    @PostMapping("/create")
    public ResponseEntity<Object> createVoucher(@RequestBody CVoucher cVoucher) {
        Optional<CVoucher> vouchers = iVoucherRepository.findById(cVoucher.getId());

        if (vouchers.isPresent()) {
            return null;
        } else {
            CVoucher voucher_create = iVoucherRepository.save(cVoucher);
            return new ResponseEntity<>(voucher_create, HttpStatus.CREATED);
        }

    }

    // update voucher
    @PutMapping("/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateVoucherById(@PathVariable("id") Long id,
            @RequestBody CVoucher voucher) {
        Optional<CVoucher> voucher1 = iVoucherRepository.findById(id);
        if (voucher1.isPresent()) {
            CVoucher voucher2 = voucher1.get();
            voucher2.setMaVoucher(voucher.getMaVoucher());
            voucher2.setGhiChu(voucher.getGhiChu());
            voucher2.setPhanTramGiamGia(voucher.getPhanTramGiamGia());
            voucher2.setGhiChu(voucher.getGhiChu());
            voucher2.setNgayCapNhat(voucher.getNgayCapNhat());

            CVoucher voucher3 = iVoucherRepository.save(voucher2);
            try {
                return new ResponseEntity<>(voucher3, HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Provice:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Provice: " + id + "  for update.");
        }
    }

    // delete voucher bởi id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<CVoucher> deleteWardById(@PathVariable("id") Long id) {
        try {
            iVoucherRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }

}
