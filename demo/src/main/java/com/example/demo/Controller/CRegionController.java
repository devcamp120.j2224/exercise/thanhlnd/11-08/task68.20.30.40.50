package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CCountry;
import com.example.demo.Model.CRegion;
import com.example.demo.Respository.iCountryRespository;
import com.example.demo.Respository.iRegionRespository;

@RestController
@CrossOrigin
@RequestMapping("region")
public class CRegionController {
	@Autowired
	private iRegionRespository iregionRepository;
	
	@Autowired
	private iCountryRespository icountryRepository;
	

    // tạo region bởi id của country
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createRegion(@PathVariable (name = "id") Long id , @RequestBody CRegion cRegion) {

        Optional<CCountry> countryData = icountryRepository.findById(id);
    
        // để vầy mới ko lỗi
        Optional<CRegion> regionFound = iregionRepository.findById(id);    
        
        if(countryData.isPresent()){
            try {
                if(regionFound.isPresent()){
                    return ResponseEntity.unprocessableEntity().body(" reagion already exsit  ");
                }
        
        CCountry cCountryNew = countryData.get();
        cRegion.setCountry(cCountryNew);
        CRegion order = iregionRepository.save(cRegion);

            return new ResponseEntity<Object>(order, HttpStatus.CREATED);
        } 
        catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation about this Entity " +e.getCause().getMessage());
        }
    }    
        else {
                 return ResponseEntity.badRequest().body("Failed to get specified id: " + id);
             }
    }

	// update region bởi id region
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {

		Optional<CRegion> region = iregionRepository.findById(id);

		if(region.isPresent()){	
			CRegion newRegion = region.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());

			return new ResponseEntity<>(iregionRepository.save(newRegion), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	// xóa region by id
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			iregionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// tìm region bằng id
	@GetMapping("/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (iregionRepository.findById(id).isPresent())
			return iregionRepository.findById(id).get();
		else
			return null;
	}

    // lấy all region
	@GetMapping("/all")
    public ResponseEntity<Object> getAllorders(){
        List<CRegion> regionList = new ArrayList<CRegion>();
        try {
            iregionRepository.findAll().forEach(orderElement -> {
                regionList.add (orderElement);
            });
            return new ResponseEntity<Object>(regionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }
	// tìm region bởi countryid 
	@GetMapping("/country/{countryId}/regions")
    public List < CRegion > getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
        return iregionRepository.findByCountryId(countryId);
    }
	
	// tìm region bởi countryId và regionId
	@GetMapping("/country/{countryId}/regions/{id}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,@PathVariable(value = "id") Long regionId) {
        return iregionRepository.findByIdAndCountryId(regionId,countryId);
    }
}
