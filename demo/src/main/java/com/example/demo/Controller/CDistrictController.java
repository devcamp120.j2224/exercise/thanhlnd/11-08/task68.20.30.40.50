package com.example.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CDistrict;
import com.example.demo.Model.CRegion;
import com.example.demo.Respository.IDistrictRepository;
import com.example.demo.Respository.iRegionRespository;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("district")
public class CDistrictController {

    @Autowired
    IDistrictRepository iDistrictRepository;

    @Autowired
    iRegionRespository iRegionRespository ;

    @GetMapping("all")
    public ResponseEntity<List<CDistrict>> getAllDistrict() {
        try {
            List<CDistrict> listDistrict = new ArrayList<>();
            iDistrictRepository.findAll().forEach(listDistrict::add);
            if (listDistrict.size() == 0) {
                return new ResponseEntity<List<CDistrict>>(listDistrict, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CDistrict>>(listDistrict, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tìm drink bởi id
    @GetMapping("/detail")
    public ResponseEntity<Object> getDistrictById(@RequestParam(name = "id", required = true) Long id) {
        Optional<CDistrict> districtFounded = iDistrictRepository.findById(id);
        if (districtFounded.isPresent())
            return new ResponseEntity<Object>(districtFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

      // tạo district bởi id của region
      @PostMapping("/create/{id}")
      public ResponseEntity<Object> createRegion(@PathVariable (name = "id") Long id , @RequestBody CDistrict cDistrict) {
  
          Optional<CRegion> regionData = iRegionRespository.findById(id);
      
          // để vầy mới ko lỗi
          Optional<CDistrict> districtFound = iDistrictRepository.findById(cDistrict.getId());    
          
          if(regionData.isPresent()){
              try {
                  if(districtFound.isPresent()){
                      return ResponseEntity.unprocessableEntity().body(" reagion already exsit  ");
                  }
          
            CRegion regionNew = regionData.get();
            cDistrict.setRegion(regionNew);
            CDistrict order = iDistrictRepository.save(cDistrict);
  
              return new ResponseEntity<Object>(order, HttpStatus.CREATED);
          } 
          catch (Exception e) {
              return ResponseEntity.unprocessableEntity()
                  .body("Can not execute operation about this Entity " +e.getCause().getMessage());
          }
      }    
          else {
                   return ResponseEntity.badRequest().body("Failed to get specified id: " + id);
               }
      }

    // update district bằng id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDistrictById(@PathVariable("id") long id, @RequestBody CDistrict district) {
        Optional<CDistrict> updateDistrict = iDistrictRepository.findById(id);
        if(updateDistrict.isPresent()) {
            CDistrict saveDistrict = updateDistrict.get();

            saveDistrict.setName(district.getName());
            saveDistrict.setPrefix(district.getPrefix());

            try {
                return ResponseEntity.ok(iDistrictRepository.save(saveDistrict));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        }else{
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete district bằng id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<CDistrict> deleteDistrictById(@PathVariable("id") long id) {
        try {
            iDistrictRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
