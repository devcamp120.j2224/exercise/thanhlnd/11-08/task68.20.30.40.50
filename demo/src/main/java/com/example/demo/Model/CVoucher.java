package com.example.demo.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity // khai báo entity để nó tạo ra 1 bảng
@Table(name = "pizza_vouchers")
public class CVoucher {
    // khai báo 1 key iD 
    @Id 
    @GeneratedValue (strategy =  GenerationType.AUTO)
    private long id ;

    // sửa lại giá trị cho cột
    @Column(name = "phan_tram_gia_gia")
    private String phanTramGiamGia ;

    @Column(name = "ghi_chu")
    private String ghiChu ;

    @Column(name = "ma_voucher")
    private String maVoucher ;

    @Column(name = "ngay_tao")
    private long ngayTao ;

    @Column(name = "ngay_cap_nhat")
    private long ngayCapNhat;


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getPhanTramGiamGia() {
        return phanTramGiamGia;
    }
    public void setPhanTramGiamGia(String phanTramGiamGia) {
        this.phanTramGiamGia = phanTramGiamGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public String getMaVoucher() {
        return maVoucher;
    }
    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }
    public long getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }
    public long getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    public CVoucher() {

    }
    public CVoucher(long id, String phanTramGiamGia, String ghiChu, String maVoucher, long ngayTao, long ngayCapNhat) {
        this.id = id;
        this.phanTramGiamGia = phanTramGiamGia;
        this.ghiChu = ghiChu;
        this.maVoucher = maVoucher;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
