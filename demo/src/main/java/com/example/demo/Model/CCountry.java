package com.example.demo.Model;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "country")
public class CCountry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "country_name")
    private String countryName;

    @Transient
    private int regionTotal = 0;


    // đem hàm get lên để đẹp format và dễ tính 
    public int getRegionTotal(){
      
        int sum = 0;

        for(int i = 0 ; i < regions.size() ; i++){
               
                sum = regions.size();
              
        }
        return sum;
    }

    public void setRegionTotal(int regionTotal) {
        this.regionTotal = regionTotal;
    }

    @OneToMany(targetEntity = CRegion.class, cascade = CascadeType.ALL )
    @JoinColumn(name = "country_id")
    @JsonManagedReference
    private List<CRegion> regions;


    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

    
	public List<CRegion> getRegions() {
		return regions;
	}

	public void setRegions(List<CRegion> regions) {
		this.regions = regions;
	}
   

   
 
}



