package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict,Long>{
    //CDistrict findById(long id);
}
