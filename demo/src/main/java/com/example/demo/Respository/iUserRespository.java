package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CUser;

public interface iUserRespository extends JpaRepository <CUser , Long> {
    // định nghĩa lại để xài get set tìm order by user id
    CUser findById(long id) ;
}
